package com.collection.client;

import java.awt.List;

import com.collection.dto.BankAccount;
import com.collection.service.BankAccountService;
import com.collection.service.BankAccountServiceImpl;

public class BankAccountCollectionsMain {
	public static void main(String[] args) {
		BankAccountService bservice = new BankAccountServiceImpl();
		bservice.addNewBankAccount(new BankAccount(1,"A",1000));
		bservice.addNewBankAccount(new BankAccount(2,"B",2000));
		bservice.addNewBankAccount(new BankAccount(3,"C",3000));
		bservice.addNewBankAccount(new BankAccount(4,"D",5000));
		
		List actList = (List) bservice.showAllAccounts();
		for(BankAccount act : actList){
			System.out.println(act.getActNumber()+" "+act.getCustomerName()+" "+act.getActBalance());
			
		}
		BankAccount temp = new BankAccount(1,"sujeet",1000);
		bservice.updateBankAccount(temp);
		System.out.println("updated list after update operation..");
		BankAccount temp1 = new BankAccount(1,"sujeet",1000);
		bservice.removeBankAccount(temp1);
		System.out.println("list after removing the record");
		for(BankAccount act : actList){
			System.out.println(act.getActNumber()+" "+act.getCustomerName()+" "+act.getActBalance());
			
		}
		
		
		
	}

}
