package com.collection.exception;

public class AccountNotFoundException extends Exception {
	
	private static final long serialVersionUID = 1L;
	private int actNumber;

	public AccountNotFoundException() {
		super();
		
	}

	@Override
	public String toString() {
		return "AccountNotFoundException [actNumber=" + actNumber + "]";
	}

	public AccountNotFoundException(int actNumber) {
		super();
		this.actNumber = actNumber;
	}
	

}
