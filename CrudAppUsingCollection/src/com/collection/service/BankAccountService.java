package com.collection.service;

import java.util.List;

import com.collection.dto.BankAccount;


public interface BankAccountService {
	public abstract void addNewBankAccount(BankAccount bankAccount);
	public abstract void removeBankAccount(BankAccount bankAccount);
	public abstract void updateBankAccount(BankAccount bankAccount);
	public abstract List<BankAccount> showAllAccounts();
}
