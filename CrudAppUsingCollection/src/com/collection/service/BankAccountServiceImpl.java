package com.collection.service;

import java.util.List;

import com.collection.dao.BankAccountDAO;
import com.collection.dao.BankAccountDAOImpl;
import com.collection.dto.BankAccount;

public class BankAccountServiceImpl implements BankAccountService {
    BankAccountDAO bdao = new BankAccountDAOImpl();
	@Override
	public void addNewBankAccount(BankAccount bankAccount) {
		bdao.addNewBankAccount(bankAccount);
		
	}

	@Override
	public void removeBankAccount(BankAccount bankAccount) {
		bdao.removeBankAccount(bankAccount);
		
	}

	@Override
	public void updateBankAccount(BankAccount bankAccount) {
		bdao.updateBankAccount(bankAccount);
		
	}

	@Override
	public List<BankAccount> showAllAccounts() {
		
		return bdao.showAllAccounts();
	}

}
