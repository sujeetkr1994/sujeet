package com.model;

import java.util.Set;

public class Department {
	private int depNo;
	private String deptName;
    private Set<Student> students;
	public Department() {
		super();
	}
	public Department(int depNo, String deptName, Set<Student> students) {
		super();
		this.depNo = depNo;
		this.deptName = deptName;
		this.students = students;
	}
	
	
	public int getDepNo() {
		return depNo;
	}
	public void setDepNo(int depNo) {
		this.depNo = depNo;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public Set<Student> getStudents() {
		return students;
	}
	public void setStudents(Set<Student> students) {
		this.students = students;
	
}
}
