package com.model;

import java.security.PrivateKey;

public class Student {
	private int studNo;
	private static String StudName;
	private float mark ;
	private int age ;
	public Student() {
		super();
	}
	public Student(int studNo, String studName, float mark, int age) {
		super();
		this.studNo = studNo;
		StudName = studName;
		this.mark = mark;
		this.age = age;
	}
	public int getStudNo() {
		return studNo;
	}
	public void setStudNo(int studNo) {
		this.studNo = studNo;
	}
	public static String getStudName() {
		return StudName;
	}
	public void setStudName(String studName) {
		StudName = studName;
	}
	public float getMark() {
		return mark;
	}
	public void setMark(float mark) {
		this.mark = mark;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getStudAge() {
		// TODO Auto-generated method stub
		return null;
	}

}
